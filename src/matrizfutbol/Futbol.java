package matrizfutbol;

/**
 *
 * @author npedrosapena
 */
public class Futbol
{
    /*
    ñlakjdlkqwiueojkcvvj klajñlkjladksjfdkljsdfalkjadslñkfjdsvnnaldjadsjklaksdjfjkasd
    lañkjdlñfajsdljadsljfljuvoiuoique4oiur
    q2
    tgrgrgqwr
    qljgñljrjtqu4ijfvk
    qñlrjfñlajfñlkjñqwer
    
    */
    private String equipos[]=
    {
        "R.C. Chindasvinto",
        "Atl. Club de Cuspridiños",
        "Cañita Brava F.C.",
        "R.C. José Tojeiro",
        "Drogha no colacau F.C."
    };
    
    private String jornadas[]=
    {
        "Jornada 1",
        "Jornada 2",
        "Jornada 3",
        "Jornada 4"
    };
    
    private String [][] arrayResultados= new String[this.equipos.length][this.jornadas.length];

    /**
     * @return the equipos
     */
    public String[] getEquipos ()
    {
        return equipos;
    }

    /**
     * @param equipos the equipos to set
     */
    public void setEquipos ( String[] equipos )
    {
        this.equipos = equipos;
    }

    /**
     * @return the jornadas
     */
    public String[] getJornadas ()
    {
        return jornadas;
    }

    /**
     * @param jornadas the jornadas to set
     */
    public void setJornadas ( String[] jornadas )
    {
        this.jornadas = jornadas;
    }
    
    /**
     * crea un array con los goles de la jornada de cada equipo
     * 
     * @return array con los resultados 
     */
    public String [][] creaJornadas()
    {
        
        
        for (int filas=0;filas<this.equipos.length;filas++)
        {
            //arrayResultados[filas][0]=this.equipos[filas];
            for (int columnas=0;columnas<this.jornadas.length;columnas++)
            {
                arrayResultados[filas][columnas]=Integer.toString ((int)Math.floor ( Math.random ()*5));
            } 
        }
        
        this.ver ( arrayResultados );
        
        return arrayResultados;   
    }
    
    
    /**
     * para visualizar los elementos del array
     * 
     * @param arrayResultados 
     */
    public void ver(String arrayResultados[][])
    {
        int contador=0;
        for(int filas=0;filas<arrayResultados.length;filas++)
        {
            System.out.println ( "equipo: "+this.equipos[filas]);
            contador=-1;
            for(String elementos:arrayResultados[filas])
            {
                System.out.println ( "goles: "+elementos+" jornada: "+this.jornadas[contador+=1] );
            }
            System.out.println ( "" );
        }
    }
    
    /**
     * muestra los goles totales de cada equipo
     */
    public void golesTotalesEquipo(String arrayResultados[][])
    {
        int acumuladorGoles=0;
        
        for (int filas=0;filas<this.equipos.length;filas++)
        {
            acumuladorGoles=0;
            
           for (int columnas=0;columnas<this.jornadas.length;columnas++)
           {
               acumuladorGoles+=Integer.parseInt ( arrayResultados[filas][columnas]);
           } 
            System.out.println ( "total equipo "+this.equipos[filas]+": "+acumuladorGoles );
        }
    }
        
    
    /**
     * muestra los goles totales marcador en cada jornada
     * 
     * @param arrayResultados 
     */
        public void golesTotalesJornada(String arrayResultados[][])
        {
            int acumuladorGoles=0;
            
            for (int columnas=0;columnas<this.jornadas.length;columnas++)
            {
                acumuladorGoles=0;
                for(int filas=0;filas<this.equipos.length;filas++)
                {
                    acumuladorGoles+=Integer.parseInt ( arrayResultados[filas][columnas]);
                }
                
                 System.out.println ( "total "+this.jornadas[columnas]+": "+acumuladorGoles );
            }
 
        }
        
        /**
         * dada una jornada, imprime el equipo máximo goleador
         * 
         * @param arrayResultados
         * @param jornada 
         */
        public void maximoGoleador(String arrayResultados[][],int jornada)
        {
            int columnas=jornada-1;
            int indice=-1;
            int control=0;
            int comparador=Integer.parseInt (arrayResultados[0][columnas]);
            
            for(int filas=0;filas<this.equipos.length;filas++)
            {
                indice+=1;
                
               if (comparador<Integer.parseInt (arrayResultados[filas][columnas]))
               {
                   comparador=Integer.parseInt ( arrayResultados[filas][columnas]);
                   control=indice;
               }
            }
                System.out.println ( "el maximo goleador fué: "+this.equipos[control]+" con "+arrayResultados[control][columnas]+" goles" );
        }
        
        public void ordenarPorGoles(String arrayResultados[][])
        {
            int arrayAux[]=new int[this.equipos.length];
            int acumuladorGoles;
            int arrayPosiciones[]=new int[this.equipos.length];
            int auxiliar;
            
            for (int filas=0;filas<this.equipos.length;filas++)
            {
                acumuladorGoles=0;

               for (int columnas=0;columnas<this.jornadas.length;columnas++)
               {
                   acumuladorGoles+=Integer.parseInt ( arrayResultados[filas][columnas]);
               } 
               
               arrayAux[filas]=acumuladorGoles;   
            }
            
            //for(int pos=0;)
            

                 int aux;
                 int posicion=-1;
                 int posicionesArray[]=new int[this.equipos.length];
                 boolean mayor=false;
                 boolean paso=false;
                 
                 for (int i=0; i<arrayAux.length-1;i++)
                 {
                     posicionesArray[i]=i;
                     
                     for ( int j=i+1; j<arrayAux.length;j++ )
                     {
                         //mayor=false;
                        if(arrayAux[i]>arrayAux[j])
                        {
                           // mayor=true;
                            aux=arrayAux[i];
                            arrayAux[i]=arrayAux[j];
                            arrayAux[j]=aux;
                            posicionesArray[i]=j;
                           // paso=true;
                        } 
                        
                     /*   if(!mayor)
                        {
                            mayor=false;
                            if (paso)
                            {paso=false;
                                posicionesArray[i]=j-1;
                            }else
                            {
                                posicionesArray[i]=i;
                            }
                            paso=false;
                        }*/
                     }
                 }
               
                 
            
            posicion=-1;
            for(int elementos:arrayAux)
         {
             posicion++;
             System.out.println ( "equipo: "+this.equipos[posicionesArray[posicion]] );
             System.out.println ( elementos );
         }
            for (int indices:posicionesArray)
            {
                System.out.println ("indices: "+ indices );
            }
           //falta ordenar el array de resultados
        }
}
